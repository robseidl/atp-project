import os
from random import randint
import flask

import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go
from plotly import tools
import dill
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

print(dcc.__version__) # 0.6.0 or above is required

# Setup the app
# Make sure not to change this file name or the variable names below,
# the template is configured to execute 'server' on 'app.py'
server = flask.Flask(__name__)
server.secret_key = os.environ.get('secret_key', str(randint(0, 1000000)))
app = dash.Dash(__name__, server=server)


ext_url = "https://codepen.io/seidlr/pen/yXdove.css"
app.css.append_css({
    "external_url": ext_url
})

# Modeling, load data
X = dill.load(open('ml-data/modeling/general_X.dill','rb'))
Y = dill.load(open('ml-data/modeling/general_Y.dill','rb'))
feature_importance = dill.load(open('ml-data/modeling/feature_importance.dill','rb'))

# Player performance data
df_player_stats = dill.load(open('ml-data/player_performance/df_player_stats.dill','rb'))
labels_g = dill.load(open('ml-data/player_performance/cluster_labels_general','rb'))
labels_g = labels_g[:21]

# Age-performance data

[age_performance,grid,quad] = dill.load(open('ml-data/age_performance/age_performance.dill','rb'))

# Prediction load


[playerInfoDict,labelsDict,featuresDict] = dill.load(open('ml-data/modeling/fullData.dill','rb'))
atp2017_prediction = dill.load(open('ml-data/modeling/atp2017_pred.dill','rb'))
atp2017_prediction_short = atp2017_prediction.iloc[[0,3,4,7,11,23,24,33]]


# Misc load


data_krumm =  pd.read_pickle('ml-data/misc/krumm.pkl')
data_muster =  pd.read_pickle('ml-data/misc/muster.pkl')

data_krumm['winner_age'] = data_krumm['winner_age'].astype(int)
data_krumm['loser_age'] = data_krumm['loser_age'].astype(int)

data_muster['winner_age'] = data_muster['winner_age'].astype(int)
data_muster['loser_age'] = data_muster['loser_age'].astype(int)



# Model-performance
model_performance = dill.load(open('ml-data/modeling/general_model_performance.dill','rb'))

X.columns = ['opp1_acePct', 'opp1_dfPct', 'opp1_1stServePct', 'opp1_2ndServePct', 'opp1_2ndServeInPct', 'opp1_1stServePointsWonPct', 'opp1_2ndServePointsWonPct', 'opp1_breakPointsFacedPct', 'opp1_breakPointsSavedPct', 'opp2_acePct', 'opp2_dfPct', 'opp2_1stServePct', 'opp2_2ndServePct', 'opp2_2ndServeInPct', 'opp2_1stServePointsWonPct', 'opp2_2ndServePointsWonPct', 'opp2_breakPointsFacedPct', 'opp2_breakPointsSavedPct']

data_model = go.Bar(
        x=model_performance['Model'], # assign x as the dataframe column 'x'
        y=model_performance['Accuracy']
    )

data_model = go.Bar(
        x=model_performance['Model'], # assign x as the dataframe column 'x'
        y=model_performance['Accuracy'],
        showlegend=False,
        hoverinfo="none"
    )

data_feature = []
for col in feature_importance.columns:
    data_feature.append(  go.Bar( x =feature_importance.index.tolist(), y=feature_importance[col], name=col, showlegend=True ) )


fig_performance = tools.make_subplots(rows=1, cols=2)
fig_performance.append_trace(data_model, 1, 1)
'''fig_performance['layout'].update(yaxis = dict(tickfont=dict(color='#ccc')),
        xaxis = dict(titlefont=dict(color='#ccc'),
                     tickfont=dict(color='#ccc')),
        legend=dict(font=dict(color='#ccc')))'''
for feat in data_feature:
    fig_performance.append_trace(feat, 1, 2)

fig_performance['layout'].update(barmode='group')
fig_performance['layout'].update(annotations=[
        dict(x=xi,y=yi-0.1,
             text='{:4.1f} %'.format(yi*100),
             xanchor='center',
             yanchor='bottom',
             showarrow=False,
        ) for xi, yi in zip(model_performance['Model'], model_performance['Accuracy'])],
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)')

fig_performance['layout'].update(margin={'b': 200, 't': 10, 'r': 10})


fig_performance = {
    "config":{"displayModeBar": False},
    "layout": fig_performance['layout'],
    "data": fig_performance['data']
}

# Init clustering


[playerInfoDict,labelsDict,featuresDict] = dill.load(open('ml-data/clustering/fullData.dill','rb'))


pca = PCA(n_components=2)

clusterList = [3,4,5,6]

clusters = []
for n_clusters in clusterList:
    datasets = {}
    for key in labelsDict:


        reduced_data = pca.fit_transform(featuresDict[key])

        kmeans = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
        kmeans.fit(reduced_data)


        # Plot the decision boundary. For that, we will assign a color to each
        h = .0005
        x_min, x_max = reduced_data[:, 0].min() - 0.025, reduced_data[:, 0].max() + 0.025
        y_min, y_max = reduced_data[:, 1].min() - 0.025, reduced_data[:, 1].max() + 0.025
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Obtain labels for each point in mesh. Use last trained model.
        Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape[0],yy.shape[1])


        centroids = kmeans.cluster_centers_

        short_names = [label.split()[-1] for label in labelsDict[key]]

        datasets[key] = {'data': reduced_data,
                         'contour': Z,
                         'xx': xx,
                         'yy': yy,
                         'short_names': short_names,
                         'centroids': centroids}
        print('Finished computation for {} and clustering {}'.format(key, n_clusters))

    clusters.append(datasets)




player_images = ['bautista', 'berdych', 'carreno', 'cilic', 'cuevas', 'dimitrov', 'djokovic', 'federer', 'fognini', 'gasquet', 'goffin', 'isner', 'karlovic', 'kyrgios', 'lopez', 'monfils', 'muller', 'murray', 'nadal', 'nishikori', 'potro', 'pouille', 'querrey', 'ramos', 'raonic', 'sock', 'thiem', 'tsonga', 'wawrinka', 'zverev', 'zverev']






# Since we're adding callbacks to elements that don't exist in the app.layout,
# Dash will raise an exception to warn us that we might be
# doing something wrong.
# In this case, we're adding the elements through a callback, so we can ignore
# the exception.
app.config.supress_callback_exceptions = True

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])



# Model performance Layout
model_performance_layout = html.Div(children=[
    dcc.Graph(
        id='performance',
        figure= fig_performance
    )])


# Player performance
dates = ['2010','2011','2012','2013','2014','2015','2016']


player_performance_layout = html.Div([
    html.Div([
        html.Div([
            dcc.Slider(
                id='year-slider',
                min=2010,
                max=2016,
                value=2010,
                step=None,
                marks={str(year): year for year in dates},
            )

        ], style={'width': '40%', 'display': 'inline-block', 'margin': 'auto', 'padding': '20px 5px'}),
        html.Div([
            dcc.Dropdown(
                id='player-dropdown',
                options=[{'label': player, 'value': player} for player in labels_g],
                value='Stanislas Wawrinka'
            )], style={'width': '40%', 'display': 'inline-block', 'margin': 'auto', 'padding': '20px 5px'})
    ], style={'margin': 'auto', 'padding': '0px 30px'}),
    dcc.Graph(
        id='performance-graph'
    )

])


@app.callback(
    dash.dependencies.Output('performance-graph', 'figure'),
    [dash.dependencies.Input('year-slider', 'value'),
     dash.dependencies.Input('player-dropdown', 'value')])
def update_figure(selected_year, selected_player):
    stat_cols = ['acePct', 'dfPct', '1stServePct', '2ndServePct', '2ndServeInPct', '1stServePointsWonPct',
                 '2ndServePointsWonPct', 'breakPointsSavedPct']

    player_group = df_player_stats[df_player_stats['year'] == int(selected_year)].groupby('player')[stat_cols]

    data_ = []

    for col in player_group.get_group(selected_player).columns:
        if col != 'surface':
            data_.append(go.Box(y=player_group.get_group(selected_player)[col], name=col, hoverinfo="y+name", showlegend=True))

    return {
        "config":{"displayModeBar": False},
        'data': data_,
        'layout': go.Layout(
            margin={'l': 40, 'b': 90, 't': 10, 'r': 10},
            hovermode='closest',
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis=dict(
                titlefont=dict(
                    color='#ccc'
                ),
                tickfont= dict(color='#ccc'),
            ),
            yaxis=dict(
                    titlefont=dict(
                        color='#ccc'
                    ),
                tickfont=dict(color='#ccc'),
            ),
            legend=dict(
                font=dict(
                    color='#ccc'
                )
            )
        )
    }


# Clustering layout


def generate_player_info_cluster(player_name, dataframe):

    txtColumns = ['winner_hand', 'winner_ht', 'winner_ioc']
    txtColumnsName = ['Hand:', 'Height:', 'Nationality:']
    matchColumns = ['acePct', 'dfPct', '1stServePct', '2ndServePct', '2ndServeInPct', '1stServePointsWonPct',
                    '2ndServePointsWonPct', 'breakPointsSavedPct']
    matchColumnNames = ['Ace', 'Doublefault', '1stServe', '2ndServe', '2ndServeIn', '1stServePointsWon',
                    '2ndServePointsWon', 'breakPointsSaved']

    trace = go.Bar(
        x= [round(dataframe.iloc[0][col]*100,1) for col in dataframe[matchColumns].columns],
        y= matchColumnNames,
        marker=dict(
            color='rgba(50, 171, 96, 0.9)',
            line=dict(
                color='rgba(50, 171, 96, 1.0)',
                width=1),
        ),
        name='Player info',
        orientation='h',
        hoverinfo="x"
    )

    short_name = player_name.split()[-1].lower()
    img_src = ""

    if short_name in player_images:
        img_src = "https://s3.eu-central-1.amazonaws.com/seidlr-capstone-atp/images/{}_m.jpg".format(short_name)
    else:
        img_src = "https://s3.eu-central-1.amazonaws.com/seidlr-capstone-atp/images/unknown.jpg"

    return [
    html.Div(className="row", children=[
    html.Div(className="one-third column", children=[
        html.H2(player_name),
        html.Table(
        [html.Tr([
            html.Td(txtColumnsName[i]),html.Td(dataframe.iloc[0][col])
        ]) for i,col in enumerate(dataframe[txtColumns].columns)]
    ), dcc.Graph(
        id="player-chart",
        figure={"config":{"displayModeBar": False},
                "data": [trace],
              "layout": go.Layout(
                        paper_bgcolor='rgba(0,0,0,0)',
                        plot_bgcolor='rgba(0,0,0,0)',
                        margin={'l': 125, 'b': 40, 't': 40, 'r': 30},
                        hovermode='closest',
                        showlegend=False,
                          xaxis=dict(
                              showgrid=False,
                              showline=True,
                              showticklabels=True,
                              zeroline=True,
                              ticksuffix= '%',
                              showticksuffix= 'last',
                              range=[0,100],
                              tickfont=dict(color='#ccc'),
                              titlefont=dict(color='#ccc'),
                          ),
                          autosize=False,
                          width=300,
                          height=200,
                  yaxis=dict(
                      titlefont=dict(
                          color='#ccc'
                      ),
                      tickfont=dict(color='#ccc'),
                  ),
                  legend=dict(
                      font=dict(
                          color='#ccc'
                      )
                  )


                    )

                }
    )
    ]),
        html.Div(className="seven columns", children=[
            html.Img(width="100px", src=img_src, style={'float': 'right'})
        ], style={'float': 'right'})
        ])
    ]

clustering_layout = html.Div(className="container",children=[
    html.Div(className='row', children=[
        html.H1('Player clustering'),
        html.Div(children='''Inspect the similarity of tour players on different surfaces.''')
    ]),

html.Div(className="row", children=[
        html.Div(className="seven columns", children=[
            html.Div('Court type:', style={'width': '40%', 'display': 'inline-block', 'padding': '20px 20px', 'margin-left': '20px'}),
            html.Div('Number of clusters:', style={'width': '40%', 'display': 'inline-block'})
        ])

    ]),
    html.Div(className="row", children=[
        html.Div(className="seven columns", children=[
                html.Div([
                    dcc.Dropdown(
                                id='surface-dropdown',
                                options=[
                                    {'label': 'All', 'value': 'general'},
                                    {'label': 'Grass', 'value': 'grass'},
                                    {'label': 'Clay', 'value': 'clay'},
                                    {'label': 'Hard', 'value': 'hard'}
                                ],
                                value='general'
                    )
                    ], style={'width': '40%', 'display': 'inline-block', 'padding': '0px 20px'}),
                    html.Div([
                        dcc.Slider(
                                        id='cluster-slider',
                                        min=min(clusterList),
                                        max=max(clusterList),
                                        value=5,
                                        step=None,
                                        marks={str(cluster): cluster for cluster in clusterList})
                        ], style={'width': '40%', 'display': 'inline-block', 'padding': '0px 20px','margin-bottom': '20px'})

        ])

    ], style={'padding': '0px 20px'}),

        html.Div(className="row", children=[
                html.Div(className="seven columns", children = [
                    dcc.Graph(
                        id='clustering',
                    )

                ]),
                html.Div(id="player-info",className="four columns", style={'margin':'40px 20px'})
                ]),





])



@app.callback(
    dash.dependencies.Output('player-info', 'children'),
    [dash.dependencies.Input('clustering', 'clickData'),
     dash.dependencies.Input('surface-dropdown', 'value')])
def display_hover_data(clickData, surface):
    full_name = clickData['points'][0]['customdata']

    return generate_player_info_cluster(full_name,playerInfoDict[surface].loc[[full_name]])


@app.callback(
    dash.dependencies.Output('clustering', 'figure'),
    [dash.dependencies.Input('cluster-slider', 'value'),
     dash.dependencies.Input('surface-dropdown', 'value')])
def update_clustering(ncluster,surface):

    minCluster = min(clusterList)
    cluster = ncluster - minCluster

    trace1  = [
        go.Contour(
            x=clusters[cluster][surface]['xx'][0,:],
            y=clusters[cluster][surface]['yy'][:,0],
            z=clusters[cluster][surface]['contour'],
            opacity=0.8,
            showscale=False,
            hoverinfo= "none"
        ),
        go.Scatter(x=clusters[cluster][surface]['centroids'][:, 0],
                   y=clusters[cluster][surface]['centroids'][:, 1],
                   showlegend=False,
                   mode='markers',
                   hoverinfo="none",
                   marker=dict(color='green', size=16,
                               line=dict(color='black',
                                         width=1),
                               symbol='star')
                   ),
        go.Scatter(x=clusters[cluster][surface]['data'][:, 0], y=clusters[cluster][surface]['data'][:, 1],
                   mode='markers+text',
                   text=clusters[cluster][surface]['short_names'],
                   customdata=labelsDict[surface],
                    textfont={
                        "color": "#000",
                        "family": "Arial, sans-serif",
                        "size": 20
                        },
                   textposition="top right",
                   hoverinfo="text",
                   showlegend=False,
                   marker=dict(
                       color='orange',
                       size=12,
                       showscale=False,
                       opacity=0.9
                   ))

    ]
    layout = go.Layout(
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
        xaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            autotick=True,
            ticks='',
            showticklabels=False,
            titlefont=dict(
                color='#ccc'
            ),
            tickfont=dict(color='#ccc'),
        ),
        yaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            autotick=True,
            ticks='',
            showticklabels=False,
            titlefont=dict(
                color='#ccc'
            ),
            tickfont=dict(color='#ccc')
        ),
        legend=dict(
            font=dict(
                color='#ccc'
            )
        )
   )
    return {
        "config": {"displayModeBar": False},
        'data': trace1,
        'layout': layout
    }


# Age-performance layout

trace0 = go.Scatter(
    x = age_performance.index,
    y = age_performance.values,
    mode = 'markers',
    hoverinfo="none",
    marker = dict(
        size = 10
    ),
    showlegend=False
)

trace1 = go.Scatter(
    x = grid,
    y = quad,
    mode = 'lines',
    name="performance",
    hoverinfo="x+y",
    line = dict(
            color='red',
            width = 6
        )
)

age_performance_layout = html.Div(children=[
                    dcc.Graph(
                        id='age-performance',
                        figure= {"config":{"displayModeBar": False},
                                "data": [trace0, trace1],
                                "layout": dict(title = 'Performance of ATP Tour players by age (2010-2016)',
                                              yaxis = dict(zeroline = False,
                                                           title='winning probability per game',
                                                           size=18,
                                                           titlefont=dict(
                                                               color='#ccc'
                                                           ),
                                                           tickfont=dict(color='#ccc')
                                                           ),
                                              xaxis = dict(zeroline = False,
                                                           title='age',
                                                           size=18,
                                                           titlefont=dict(
                                                               color='#ccc'
                                                           ),
                                                           tickfont=dict(color='#ccc')
                                                           ),
                                              paper_bgcolor='rgba(0,0,0,0)',
                                              plot_bgcolor='rgba(0,0,0,0)',
                                                legend=dict(
                                                                font=dict(
                                                                    color='#ccc'
                                                                )
                                                            )
                                     )}

                    )

])


# Prediction layout


player_images = ['bautista', 'berdych', 'carreno', 'cilic', 'cuevas', 'dimitrov', 'djokovic', 'federer', 'fognini', 'gasquet', 'goffin', 'isner', 'karlovic', 'kyrgios', 'lopez', 'monfils', 'muller', 'murray', 'nadal', 'nishikori', 'potro', 'pouille', 'querrey', 'ramos', 'raonic', 'sock', 'thiem', 'tsonga', 'wawrinka', 'zverev', 'zverev']

def generate_player_info(player_name, dataframe, div):

    txtColumns = ['hand', 'ht', 'ioc']
    txtColumnsName = ['Hand:', 'Height:', 'Nationality:']
    matchColumns = ['acePct', 'dfPct', '1stServePct', '2ndServePct', '2ndServeInPct', '1stServePointsWonPct',
                    '2ndServePointsWonPct', 'breakPointsSavedPct']
    matchColumnNames = ['Ace', 'Doublefault', '1stServe', '2ndServe', '2ndServeIn', '1stServePointsWon',
                    '2ndServePointsWon', 'breakPointsSaved']

    trace = go.Bar(
        x= [round(dataframe.iloc[0][col]*100,1) for col in dataframe[matchColumns].columns],
        y= matchColumnNames,
        marker=dict(
            color='rgba(50, 171, 96, 0.9)',
            line=dict(
                color='rgba(50, 171, 96, 1.0)',
                width=1),
        ),
        name='Player info',
        orientation='h',
        hoverinfo="x"
    )


    short_name = player_name.split()[-1].lower()
    img_src = ""

    if short_name in player_images:
        print("https://s3.eu-central-1.amazonaws.com/seidlr-capstone-atp/images/{}_m.jpg".format(short_name))
        img_src = "https://s3.eu-central-1.amazonaws.com/seidlr-capstone-atp/images/{}_m.jpg".format(short_name)
    else: img_src = "https://s3.eu-central-1.amazonaws.com/seidlr-capstone-atp/images/unknown.jpg"

    return [
        html.H2(player_name),
        html.Div(className="one-third column", children=[
       html.Table(
        [html.Tr([
            html.Td(txtColumnsName[i]),html.Td(dataframe.iloc[0][col])
        ]) for i,col in enumerate(dataframe[txtColumns].columns)]
    ), dcc.Graph(
        id="player-chart-{}".format(div),
        figure={"config":{"displayModeBar": False},
              "data": [trace],
              "layout": go.Layout(
                        paper_bgcolor='rgba(0,0,0,0)',
                        plot_bgcolor='rgba(0,0,0,0)',
                        margin={'l': 125, 'b': 40, 't': 40, 'r': 30},
                        hovermode='closest',
                        showlegend=False,
                          xaxis=dict(
                              showgrid=False,
                              showline=True,
                              showticklabels=True,
                              zeroline=True,
                              ticksuffix= '%',
                              showticksuffix= 'last',
                              range=[0,100],
                              tickfont=dict(color='#ccc'),
                              titlefont=dict(color='#ccc'),
                          ),
                          autosize=False,
                          width=300,
                          height=200,
                  yaxis=dict(
                      titlefont=dict(
                          color='#ccc'
                      ),
                      tickfont=dict(color='#ccc'),
                  ),
                  legend=dict(
                      font=dict(
                          color='#ccc'
                      )
                  )


                    )

                }
    )
        ]),
        html.Div(className="seven columns", children=[
            html.Img(width="100px", src=img_src, style={'float': 'right'})
        ], style={'float': 'right'})



    ]

prediction_layout = html.Div(className="container",children=[
    html.Div(className='row', children=[
        html.Div(children='''Predict the winner of an ATP Tour game''')
    ]),
    html.Div(className="row", children=[
        html.Div(className="seven columns", children=[
                html.Div(children = [
                    html.H4('Select game:'),
                    dcc.RadioItems(
                                id='playerOne-dropdown',
                                options=[{'label': "{} vs. {} : {}".format(row['winner_name'],row['loser_name'],row['score']), 'value': i} for i,row in atp2017_prediction_short.iterrows()
                                ],
                                value=0
                    )
                    ], style={'padding': '0px 20px'}),
        ])

    ], style={'padding': '0px 20px'}),

        html.Div(className="row", children=[
                html.Div(className="five columns", children = [
                    html.Div(id="playerOne-info",className="row")
                ]),
                html.Div(id="middle",className="two columns",children='VS'),
                html.Div(className="five columns",children=[
                    html.Div(id="playerTwo-info", className="row")
                ], style={'padding-left':'16'}),


        ])


])

@app.callback(
    dash.dependencies.Output('middle', 'children'),
    [dash.dependencies.Input('playerOne-dropdown', 'value')])
def display_hover_data(game_index):
    if game_index!="":
        row = atp2017_prediction_short.loc[[game_index]]
        pOpp1 = row['pred_winner_prob'].item()   # winner is opponent 2 ( y == 1)
        pOpp2 = row['pred_loser_prob'].item()
        predCorrect = row['pred_correct'].item()

        return html.Div(children=[
            html.Div(row['winner_name'].item()),
            html.Div("{:2.1f} % VS {:2.1f} %".format(pOpp1*100,pOpp2*100)),
            html.Div(row['loser_name'].item())
        ], style={'text-align':'center','background': 'green' if predCorrect else 'red','width':'140'})
    return 'VS'

@app.callback(
    dash.dependencies.Output('playerOne-info', 'children'),
    [dash.dependencies.Input('playerOne-dropdown', 'value')])
def display_hover_data(game_index):
    if game_index!="":
        row = atp2017_prediction_short.loc[[game_index]]
        full_name = row['winner_name'].item()
        cols = ['winner_name', 'winner_ht', 'winner_age','winner_ioc','winner_hand', 'w_acePct', 'w_dfPct',
         'w_1stServePct', 'w_2ndServePct', 'w_2ndServeInPct', 'w_1stServePointsWonPct', 'w_2ndServePointsWonPct',
         'w_breakPointsFacedPct', 'w_breakPointsSavedPct']
        player_info = row[cols]
        player_info.columns =  ['name', 'ht', 'age','ioc','hand', 'acePct', 'dfPct',
         '1stServePct', '2ndServePct', '2ndServeInPct', '1stServePointsWonPct', '2ndServePointsWonPct',
         'breakPointsFacedPct', 'breakPointsSavedPct']


        return generate_player_info(full_name,player_info,'One')
    return None


@app.callback(
    dash.dependencies.Output('playerTwo-info', 'children'),
    [dash.dependencies.Input('playerOne-dropdown', 'value')])
def display_hover_data(game_index):
    if game_index != "":
        row = atp2017_prediction_short.loc[[game_index]]
        full_name = row['loser_name'].item()
        cols = ['loser_name', 'loser_ht', 'loser_age','loser_ioc','loser_hand', 'l_acePct', 'l_dfPct',
         'l_1stServePct', 'l_2ndServePct', 'l_2ndServeInPct', 'l_1stServePointsWonPct', 'l_2ndServePointsWonPct',
         'l_breakPointsFacedPct', 'l_breakPointsSavedPct']
        player_info = row[cols]
        player_info.columns = ['name', 'ht', 'age', 'ioc','hand','acePct', 'dfPct',
                               '1stServePct', '2ndServePct', '2ndServeInPct', '1stServePointsWonPct',
                               '2ndServePointsWonPct',
                               'breakPointsFacedPct', 'breakPointsSavedPct']


        return generate_player_info(full_name,player_info,'Two')
    return None



# Misc layout


def generate_table(dataframe, max_rows=10):
    return html.Table(className="u-full-width",children =
        # Header
    [
        html.Thead(children=[html.Tr([html.Th(col) for col in dataframe.columns])]),
        html.Tbody(children= [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))])
    ])




misc_layout = html.Div(className="container",children=[
    html.Div(className='row', children=[
        #html.H2('Miscellaneous'),
        dcc.Markdown(children='''When working with the dataset to create some of the figures you find some interesting details. Here, I want to share two stories.''')
    ]),
    html.Div(className="row"),
    html.H3('Thomas Muster'),
    dcc.Markdown('''When creating the age-performance curve I recognized that there is a player that is more than 40 years old who only lost games.
    A quick search revealed that former world No.1 **[Thomas Muster](https://en.wikipedia.org/wiki/Thomas_Muster)** still played some matches on ATP tour in Austria back in 2011.'''),
    generate_table(data_muster),
    html.Div(className="row"),
    html.H3('Kimiko Date'),
    dcc.Markdown('''Additionally to generate the age-performance curve for ATP Tour players, I also had a look into the age distribution on WTA Tour because
    I was curious when women stop to play on WTA tour. And indeed, there is [Kimiko Date](https://en.wikipedia.org/wiki/Kimiko_Date) (born 28 September 1970), a Japanese professional tennis player
    who reached a career-high ranking of World No. 4 in 1995. After playing in her second Olympic Games, she announced her retirement on 24 September 1996.
    She returned to tennis nearly **12 years later**, announcing an unexpected comeback in April 2008. Since then, she has won several ITF titles and beaten very prominent players. Her [story](https://en.wikipedia.org/wiki/Kimiko_Date) is worth reading for sure!'''),
    generate_table(data_krumm),



])

# Index


index_page = html.Div('No content!')


# Update the index
@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/model-performance':
        return model_performance_layout
    elif pathname == '/player-performance':
        return player_performance_layout
    elif pathname == '/clustering':
        return clustering_layout
    elif pathname == '/age-performance':
        return age_performance_layout
    elif pathname == '/prediction':
        return prediction_layout
    elif pathname == '/misc':
        return misc_layout
    else:
        return index_page
    # You could also return a 404 "URL not found" page here



# Run the Dash app
if __name__ == '__main__':
    app.server.run(debug=True, threaded=True)